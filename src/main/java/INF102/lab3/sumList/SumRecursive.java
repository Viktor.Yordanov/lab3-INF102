package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        int size = list.size();
        int a=0;
    
        if(list.isEmpty() == true) {
            return 0;
    
        }else {
            a = (int) (a + list.get(0) + sum(list.subList(1, size))); 
            return  a;      
        }   
    }
    

}
