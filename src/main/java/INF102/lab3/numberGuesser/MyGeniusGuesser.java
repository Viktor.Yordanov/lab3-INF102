package INF102.lab3.numberGuesser;

import java.util.Random;

public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int lowerbound = number.getLowerbound();
        int upperbound = number.getUpperbound();

        return Guess(number, lowerbound, upperbound);

    }

  
    public int Guess(RandomNumber number, int lowerbound, int upperbound) {

        int middle = (upperbound + lowerbound) /2;
		int result = number.guess(middle);

        if (result == 0 || lowerbound == upperbound){
            return (upperbound + lowerbound) /2;
        }  
        else if (result == 1){
            return Guess(number, lowerbound, middle);

        }
        else if (result == -1){
            return Guess(number, middle + 1, upperbound);

        }

        throw new IllegalArgumentException("something went wrong");
    }

}
