package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        numbers.sort(null);
        return numbers.get(numbers.size()-1);
    }

}
